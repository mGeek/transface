![Logo TransfACE](https://bytebucket.org/mGeek/transface/raw/7e5ad6554bfe1fb73ef30a202ad84214595ae9fe/public/img/logo.png)

# TransfACE

## Dependencies

* The usual Apache/PHP/MySQL suite, with [Composer](https://getcomposer.org/)

## Installation

```
git clone https://bitbucket.org/mGeek/transface.git
cd transface
cp .env.example .env
```

Edit .env file to fit your needs.
Don't forget to create the database you've juste configured (with command line/phpMyAdmin/Adminer/what u want).

Back to the terminal:
```
composer install #Downloads Laravel and packages files
chmod 777 -R storage bootstrap/cache
php artisan key:generate
php artisan migrate #Migrates the tables to the database
php artisan storage:link
php git_version_gen.php
```

App is running !