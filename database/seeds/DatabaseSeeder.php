<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = new App\User;
        $u->name = "Simon Rubuano";
        $u->email = "s.rubuano@acat.fr";
        $u->password = Hash::make('password');
        $u->save();
    }
}
