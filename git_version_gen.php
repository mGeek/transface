<?php

echo "Generating version number from Git...\r\n";

$latest_tag = shell_exec('git describe --tags $(git rev-list --tags --max-count=1)');
$commits_count_since_last_tag = shell_exec('git rev-list `git rev-list --tags --no-walk --max-count=1`..HEAD --count');
$current_hash = shell_exec("git log --pretty=format:'%h' -n 1");
$current_branch = shell_exec("git rev-parse --abbrev-ref HEAD");

$current_version = trim($latest_tag) . '.' . trim($commits_count_since_last_tag);

file_put_contents('app/version', json_encode([
    'version' => $current_version,
    'hash' => trim($current_hash),
    'branch' => trim($current_branch),
]));

echo "Version: " . $current_version . "\r\n";