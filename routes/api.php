<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('transfer', 'TransferController@create');
Route::post('transfer/{session_hash}/file', 'TransferFileController@create');
Route::get('transfer/{session_hash}/download', 'TransferController@download');
Route::get('transfer/{session_hash}/file/{file_hash}/download', 'TransferFileController@download');
