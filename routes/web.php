<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('upload');
});

Route::auth();

Route::group(['middleware' => 'auth', 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
    Route::get('/', 'AdminController@index');
});

Route::get('download/{session_hash}', function($session_hash){
    $session = App\Session::where('hash', '=', $session_hash)->first();
    if ($session===null) return abort(404);
    
    return view('download', compact('session'));
});

Route::get('download/{session_hash}/file/{file_hash}', function($session_hash, $file_hash){
    $session = App\Session::where('hash', '=', $session_hash)->first();
    if ($session===null) return abort(404);

    $file = App\File::where('hash', '=', $file_hash)->first();
    if ($file===null) return abort(404);
    
    switch (explode('/', $file->mimetype)[0]) {
        case 'text':
            $filecontents = file_get_contents(storage_path('files/' . $file->hash . '.' . $file->original_extension));
            return view('preview/textplain', compact('session', 'file', 'filecontents'));
        default:
            return view('');
    }

    return view('download', compact('session'));
});

Route::put('/{file_name}', 'TransferController@curl'); // Catch `curl -T myfile.txt transface.acat.fr` commands
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
