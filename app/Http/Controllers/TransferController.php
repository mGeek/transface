<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\File as MFile;

use File, Zipper, Cache;

class TransferController extends Controller
{
    public function create(){
        do {
            $hash = str_random(6);
        } while(Session::where('hash', '=', $hash)->count() > 0);
        
        $session = new Session;
        $session->hash = $hash;
        $session->save();
        
        return response()->json(['success' => true, 'session' => $session], 200);
    }

    public function download($session_hash) {
        $session = Session::where('hash', '=', $session_hash)->first();
        if ($session===null) return abort(404);       

        if (count($session->files) == 1) return response()->download(storage_path('files/' . $session->files[0]->hash . '.' . $session->files[0]->original_extension), $session->files[0]->original_name . '.' . $session->files[0]->original_extension);

        if (!File::exists(storage_path('zips/transface-' . $session->hash . '.zip'))) {
            $zipper = Zipper::make(storage_path('zips/transface-' . $session->hash . '.zip'));
            foreach ($session->files as $file) {
                $zipper->addString($file->original_name . '.' . $file->original_extension, file_get_contents(storage_path('files/' . $file->hash . '.' . $file->original_extension)));
            }
            $zipper->close();
        }
        
        return response()->download(storage_path('zips/transface-' . $session->hash . '.zip'));
    }

    public function curl(Request $request, $file_name){
        set_time_limit(0);

        $temp_path = storage_path('temp/' . time() . '_' . $file_name);
        
        $putdata = fopen("php://input", "r");
        $fp = fopen($temp_path, "w");
        while ($data = fread($putdata, 1024*8)) fwrite($fp, $data);
        fclose($fp);
        fclose($putdata);

        do {
            $hash = str_random(6);
        } while(Session::where('hash', '=', $hash)->count() > 0);
        
        $session = new Session;
        $session->hash = $hash;
        $session->save();
            
        do {
            $hash = str_random(6);
        } while(MFile::where('hash', '=', $hash)->count() > 0);

        $request_file_pi = pathinfo($file_name);

        $file = new MFile;
        $file->hash = $hash;
        $file->original_name = $request_file_pi['filename'];
        $file->original_extension = $request_file_pi['extension'];
        $file->size = filesize($temp_path);
        $file->mimetype = mime_content_type($temp_path);
        $file->session()->associate($session);
        $file->save();
            
        $request_file = rename($temp_path, storage_path() . '/files/' . $file->hash . '.' . $file->original_extension);
        
        $v = Cache::remember('gitVersion', 60, function (){
            return \App\Helpers\MainHelper::gitVersion();
        });

        echo "\r\n";
        echo " _________  ___   _  _____________  _________" . "\r\n";
        echo "/_  __/ _ \/ _ | / |/ / __/ __/ _ |/ ___/ __/    " . "© MGEEK - version " . $v->version . " (" . $v->branch . ", " . $v->hash . ")" . "\r\n";
        echo " / / / , _/ __ |/    /\ \/ _// __ / /__/ _/      " . $file->original_name . "." . $file->original_extension . " (" . \App\Helpers\MainHelper::bytesToHuman($file->size) . ")" . "\r\n";
        echo "/_/ /_/|_/_/ |_/_/|_/___/_/ /_/ |_\___/___/      " . url('/download/' . $session->hash) . "\r\n";
        echo "\r\n";
        exit;

    }
}
