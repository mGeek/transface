<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\File as MFile;

class TransferFileController extends Controller
{
    public function create(Request $request, $session_hash){
        $session = Session::where('hash', '=', $session_hash)->first();
        if ($session===null) return abort(404);        

        set_time_limit(0);
        $request_file = $request->file('file');
            
        do {
            $hash = str_random(6);
        } while(MFile::where('hash', '=', $hash)->count() > 0);

        $file = new MFile;
        $file->hash = $hash;
        $file->original_name = pathinfo($request_file->getClientOriginalName(), PATHINFO_FILENAME);
        $file->original_extension = $request_file->getClientOriginalExtension();
        $file->size = $request_file->getSize();
        $file->mimetype = $request_file->getMimeType();
        $file->session()->associate($session);
        $file->save();
            
        $request_file = $request_file->move(storage_path() . '/files', $file->hash . '.' . $file->original_extension);
        
        return response()->json(['success' => true, 'file' => $file], 200);
    }

    public function download($sesssion_hash, $file_hash){
        $file = MFile::where('hash', '=', $file_hash)->first();
        if ($file===null) return abort(404);       

        return response()->download(storage_path('files/' . $file->hash . '.' . $file->original_extension), $file->original_name . '.' . $file->original_extension);
    }
}
