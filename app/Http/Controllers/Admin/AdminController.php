<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Session;
use App\File as MFile;

class AdminController extends Controller
{
    public function index(){
        $sessions = Session::get();
        $files = MFile::get();

        $disk_usage = [];
        $disk_usage['total'] = disk_total_space('/');
        $disk_usage['used'] = $disk_usage['total'] - disk_free_space('/');
        $disk_usage['used_by_transface'] = MFile::get()->sum('size');

        return view('admin/index', compact('sessions', 'files', 'disk_usage'));
    }
}
