<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    function session(){
        return $this->belongsTo(Session::class, 'session_hash', 'hash');
    }
}
