<?php

namespace App\Helpers;
use File;

class MainHelper {
    public static function gitVersion() {
        return json_decode(File::get(storage_path() . '/../app/version'));
    }

    public static function bytesToHuman($bytes) {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
        for ($i = 0; $bytes > 1024; $i++) $bytes /= 1024;
        return round($bytes, 2) . ' ' . $units[$i];
    }

    public static function faClass($ext) {
        switch($ext) {
            case 'gif':
            case 'jpeg':
            case 'jpg':
            case 'png':
                return 'fa-file-image-o';
            case 'pdf':
                return 'fa-file-pdf-o';
            case 'doc':
            case 'docx':
                return 'fa-file-word-o';
            case 'ppt':
            case 'pptx':
                return 'fa-file-powerpoint-o';
            case 'xls':
            case 'xlsx':
                return 'fa-file-excel-o';
            case 'aac':
            case 'mp3':
            case 'ogg':
            case 'flac':    
                return 'fa-file-audio-o';
            case 'avi':
            case 'flv':
            case 'mkv':
            case 'mp4':
                return 'fa-file-video-o';
            case '7z':
            case 'gz':
            case 'rar':
            case 'zip':
                return 'fa-file-zip-o';
            case 'css':
            case 'html':
            case 'js':
            case 'php':
                return 'fa-file-code-o';
            default :
                return 'fa-file-o';
        }
    }
}