<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public function files() {
        return $this->hasMany(File::class, 'session_hash', 'hash');
    }
}
