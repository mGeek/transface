<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TransfACE</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('after_styles')

</head>

<body>
      
    <div class="header">
        <div class="container">
            <img src="{{ asset('img/logo.png') }}" class="animated fadeInLeftBig" height="20">
            <div class="pull-right">
                <ul class="nav nav-right">
                @if (Auth::guest())
                    <!--<a class="btn btn-warning btn-xs" href="{{ route('login') }}">Connexion</a>-->
                    <!--<li><a href="{{ route('register') }}">Register</a></li>-->
                @else
                    <span class="btn-group">
                        <a class="btn btn-default btn-xs" href="{{ url('admin') }}"><i class="fa fa-tachometer"></i> Dashboard</a>
                        <a class="btn btn-default btn-xs" disabled="disabled" href="{{ url('admin/session') }}"><i class="fa fa-folder-open-o"></i> Sessions</a>
                        <a class="btn btn-default btn-xs" disabled="disabled" href="{{ url('admin/file') }}"><i class="fa fa-file-o"></i> Files</a>
                    </span>
                    
                    <span class="dropdown">
                        <a href="#" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Déconnexion
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </span>
                @endif
                </ul>
            </div>
        </div>
    </div>

    @yield('content')              
    
    <div class="footer text-center">
        <?php
            $v = Cache::remember('gitVersion', 60, function (){
                return App\Helpers\MainHelper::gitVersion();
            });
        ?>
        &copy; <a href="http://mgeek.fr">MGEEK</a> - <span title="{{ $v->branch }}, {{ $v->hash }}">version {{ $v->version }}</span>
    </div>

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/clipboard.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    
    @yield('after_scripts')

</body>
</html>