@extends('layout')

@section('content')
    @foreach($session->files as $file)
        <div class="panel-body">
            <div class="pull-right btn-group">
                <a href="/download/{{ $session->hash }}/file/{{ $file->hash }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                <a href="/api/transfer/{{ $session->hash }}/file/{{ $file->hash }}/download" target="_blank" class="btn btn-default"><i class="fa fa-download"></i></a>
            </div>
            <i class="fa {{ App\Helpers\MainHelper::faClass($file->original_extension) }}"></i>&nbsp;{{ $file->original_name }}.{{ $file->original_extension }}<br>
            <small class="text-muted">{{ App\Helpers\MainHelper::bytesToHuman($file->size) }}</small>
        </div>
        @if (!$loop->last)
            <div class="panel-divider"></div>
        @endif
    @endforeach
    <div class="panel-footer text-center">
        <a href="/api/transfer/{{ $session->hash }}/download" class="btn btn-warning" target="_blank">
            <i class="fa fa-cloud-download fa-2x"></i><br>
            Tout télécharger
        </a>
    </div>
@endsection