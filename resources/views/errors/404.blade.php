@extends('layout')

@section('content')
    <div class="panel-body text-center">
        <!--<i class="fa fa-frown-o fa-3x"></i><br>-->
        <span class="fa-4x">🤔</span><br>
        <big>Erreur 404</big><br>
        File Not Found<br><br>
        <a href="/" class="btn btn-warning"><i class="fa fa-home"></i> Retour à l'accueil</a>
    </div>
@endsection