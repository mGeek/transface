@extends('layout')

@section('content')
    <div class="panel-body text-center">
        <!--<i class="fa fa-frown-o fa-3x"></i><br>-->
        <span class="fa-4x">🤔</span><br>
        <big>Erreur 500</big><br>
        Internal Server Error<br><br>
        <a href="/" class="btn btn-warning"><i class="fa fa-home"></i> Retour à l'accueil</a>
    </div>    
@endsection