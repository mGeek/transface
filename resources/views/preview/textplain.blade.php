@extends('layout_header')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <i class="fa {{ App\Helpers\MainHelper::faClass($file->original_extension) }} fa-3x"></i><br><big>{{ $file->original_name }}.{{ $file->original_extension }}</big><br>
                        <small class="text-muted">{{ App\Helpers\MainHelper::bytesToHuman($file->size) }}</small>
                    </div>
                    <div class="panel-footer" style="padding: 0px;">
                        <div class="btn-group-vertical btn-block" style="border-radius: 0px;">
                            <a href="/api/transfer/{{ $session->hash }}/file/{{ $file->hash }}/download" target="_blank" style="border-radius: 0px; border-width: 0px;" class="btn btn-default btn-block"><i class="fa fa-download"></i> Télécharger</a>
                            <a href="/download/{{ $session->hash }}" style="border-width: 0px;" class="btn btn-default btn-block"><i class="fa fa-arrow-left"></i> Retour</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10">
                <pre style="padding: 0px;"><code class="{{ $file->original_extension }}">{{ $filecontents }}</code></pre>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
    <link href="{{ asset('plugins/highlight.js/styles/github.css') }}" rel="stylesheet">
@endsection

@section('after_scripts')
    <script src="{{ asset('plugins/highlight.js/highlight.pack.js') }}" type="text/javascript"></script>
    <script>
        hljs.initHighlightingOnLoad();
    </script>
@endsection