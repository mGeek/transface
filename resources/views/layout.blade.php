<!doctype html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TransfACE</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('after_styles')

</head>

<body>
    <div class="content">
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <img src="{{ asset('img/logo.png') }}" height="20">
                        </div>

                        @yield('content')              
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer text-center">
        <?php
            $v = Cache::remember('gitVersion', 60, function (){
                return App\Helpers\MainHelper::gitVersion();
            });
        ?>
        &copy; <a href="http://mgeek.fr">MGEEK</a> - <span title="{{ $v->branch }}, {{ $v->hash }}">version {{ $v->version }}</span>
    </div>

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/clipboard.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    @yield('after_scripts')

</body>
</html>