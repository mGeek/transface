@extends('layout_header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-2">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <span style="font-size: 3em;">{{ $sessions->count() }}</span><br>
                    sessions crées
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <span style="font-size: 3em;">{{ $files->count() }}</span><br>
                    fichiers téléversés
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Stockage
                </div>
                <div class="panel-body">
                    <div class="progress">
                        <div class="progress-bar" style="width:{{ ($disk_usage['used']/$disk_usage['total']*100)-($disk_usage['used_by_transface']/$disk_usage['total']*100) }}%; background-color: #bbb"></div>
                        <div class="progress-bar progress-bar-warning" style="width:{{ $disk_usage['used_by_transface']/$disk_usage['total']*100 }}%"></div>
                    </div>
                    <b>{{ App\Helpers\MainHelper::bytesToHuman($disk_usage['used']) }}</b> utilisés, dont <b>{{ App\Helpers\MainHelper::bytesToHuman($disk_usage['used_by_transface']) }}</b> par TransfACE sur <b>{{ App\Helpers\MainHelper::bytesToHuman($disk_usage['total']) }}</b>.
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
