@extends('layout')

@section('content')
    {!! Form::open(['url' => '/upload', 'files' => 'true', 'id' => 'form-upload']) !!}
        <div class="panel-body" id="form">   
            {!! Form::label('files[]', 'Fichiers :') !!}
            {!! Form::file('files[]', ['id' => 'file-select', 'multiple']) !!}
            <span class="help-block">32GiB maximum/fichier, tous formats acceptés</span>                                    
        </div>
        <div class="panel-footer text-center">
            <button id="upload-submit" class="btn btn-warning">
                <i class="fa fa-cloud-upload fa-2x"></i><br>
                Envoyer
            </button>
        </div>
    {!! Form::close() !!}
    <div class="panel-body" id="upload-progress" style="display: none">
        <div id="progress"></div>
    </div>
    <div class="panel-body text-center" id="upload-done" style="display: none">
        <i class="fa fa-check fa-3x"></i><br>
        <big>Vous avez terminé !</big><br><br>
        
        <div class="form-group">
            {!! Form::label('download-link', 'Lien de téléchargement :') !!}
            <div class="input-group">
                {!! Form::text('download-link', null, ['class' => 'form-control', 'readonly', 'id' => 'download-link']) !!}
                <span class="input-group-btn">
                    <button class="btn" type="button" data-clipboard-target="#download-link" style="height: 41px;">
                        <i class="fa fa-clipboard"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>                   
@endsection

@section('after_scripts')
    <script>
        new Clipboard('.btn');

        var form = document.getElementById('form-upload');
        var fileSelect = document.getElementById('file-select');
        var filesQueue = [];
        var sessionHash = "";

        form.onsubmit = function(event) {
            event.preventDefault();
            
            var files = fileSelect.files;
            if (files.length == 0) return;

            $('#form-upload').slideUp();
            $("#upload-progress").slideDown();

            for (var i = 0; i < files.length; i++) {
                var formData = new FormData();
                var file = files[i];
                
                var html_progress = '';
                html_progress += '<div class="pull-right" id="upload-' + i + '-status">';
                html_progress += '  <i class="fa fa-clock-o"></i> En attente';
                html_progress += '</div>';
                html_progress += file.name;
                html_progress += '<div class="progress">';
                html_progress += '  <div id="upload-' + i + '-progress" class="progress-bar progress-bar-warning progress-bar-striped active" style="width: 0%">0%</div>';
                html_progress += '</div>';
                $("#progress").append(html_progress);
                
                formData.append('file', file, file.name);
                formData.append('_token', '{{ csrf_token() }}');
                
                filesQueue.push({id: i, form: formData});
            }
            
            initSession();
        }

        function initSession(){
            $.post('/api/transfer', [], function(resp){
                sessionHash = resp.session.hash;
                $("input[name='download-link']").val("{{ URL::to('/') }}/download/" + sessionHash);
                doQueue();
            }); 
        }

        function doQueue() {
            if (filesQueue.length == 0) {
                $('#upload-progress').slideUp();
                $('#upload-done').slideDown();
                return;
            }
            
            var task = filesQueue.shift();
            
            $('#upload-' + task.id + '-status').html('<i class="fa fa-circle-o-notch fa-spin"></i> Envoi en cours');
            
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/api/transfer/' + sessionHash + '/file', true);

            xhr.upload.onprogress = function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    var percentRounded = Math.round(percentComplete * 100) + "%";
                    $('#upload-' + task.id + '-progress').width(percentRounded).html(percentRounded);
                }
            };

            xhr.onload = function () {
                if (xhr.status === 200) {
                    $('#upload-' + task.id + '-status').addClass('text-warning').html('<i class="fa fa-check-circle-o"></i> Terminé');
                } else {
                    $('#upload-' + task.id + '-status').addClass('text-danger').html('<i class="fa fa-minus-circle"></i> Erreur');
                }
                return doQueue();
            };

            xhr.send(task.form);
        };
    </script>
@endsection
